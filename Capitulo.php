<?php

require_once 'ElementoMultimedia.php';

class Capitulo extends ElementoMultimedia
{
    private $numCap;

    private $titulo;

    private $fecha;

    private $valoracion;

    public function __construct(int $numCap, string $titulo, string $fecha, float $valoracion, int $duracion)
    {
        $this->numCap = $numCap;
        $this->titulo = $titulo;
        $this->fecha = $fecha;
        $this->valoracion = $valoracion;
        $this->duracion = $duracion;
    }

    public function getValoracion(): float
    {
        return $this->valoracion;
    }
}
