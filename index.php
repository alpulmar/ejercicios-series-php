<?php

require_once 'Serie.php';

$alteredCarbon = new Serie('Altered Carbon', 'Action', );

$temp1 = new Temporada(1, 2018);
$temp2 = new Temporada(2, 2020);

$cap1 = new Capitulo(1, 'Out of the Past', '02-02-2018', 7.9, 58);
$cap2 = new Capitulo(2, 'Fallen Angel', '02-02-2018', 7.9, 56);
$cap3 = new Capitulo(3, 'In a Lonely Place', '02-02-2018', 8.1, 51);
$cap4 = new Capitulo(4, 'Force of Evil', '02-02-2018', 8.6, 49);

$cap9 = new Capitulo(9, 'Phantom Lady', '27-02-2020', 6.4, 45);

$temp1->addCapitulo($cap1);
$temp1->addCapitulo($cap2);
$temp1->addCapitulo($cap3);
$temp1->addCapitulo($cap4);

$temp2->addCapitulo($cap9);

$alteredCarbon->addTemporada($temp1);
$alteredCarbon->addTemporada($temp2);

$totalCapitulos = $alteredCarbon->getNumCapitulosPorTemporada(1);

echo "EL número total de capitulos de la temp 1 de Altered Carbon es: " . $totalCapitulos . "\n";

$valoracion = $alteredCarbon->getValoracion();

echo "La valoración media de Altered Carbon es: " . number_format($valoracion, 1);
