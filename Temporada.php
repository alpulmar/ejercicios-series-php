<?php

require_once 'Capitulo.php';

class Temporada
{
    private $numero;

    private $year;

    private $capitulos;

    public function __construct(int $numero, int $year)
    {
        $this->numero = $numero;
        $this->year = $year;
        $this->capitulos = [];
    }

    public function addCapitulo(Capitulo $capitulo)
    {
        $this->capitulos[] = $capitulo;
    }

    public function getNumero(): int
    {
        return $this->numero;
    }

    public function getTotalCapitulos()
    {
        return count($this->capitulos);
    }

    public function getCapitulos(): array
    {
        return $this->capitulos;
    }
}
