<?php

require_once 'Temporada.php';

class Serie
{
    private $nombre;

    private $genero;

    private $temporadas;

    public function __construct(string $nombre, string $genero)
    {
        $this->nombre = $nombre;
        $this->genero = $genero;
        $this->temporadas = [];

    }

    public function addTemporada(Temporada $temporada)
    {
        $this->temporadas[] = $temporada;
    }

    public function getNumCapitulosPorTemporada(int $numTemporada): int
    {
        foreach ($this->temporadas as $temporada) {
            if ($temporada->getNumero() == $numTemporada) {
                return $temporada->getTotalCapitulos();
            }
        }

        return 0;
    }

    public function getValoracion(): float
    {
        $sumaValoraciones = 0;
        $totalCapitulos = 0;

        foreach ($this->temporadas as $temporada) {
            $capitulos = $temporada->getCapitulos();
            $totalCapitulos += $temporada->getTotalCapitulos();

            foreach ($capitulos as $capitulo) {
                $sumaValoraciones += $capitulo->getValoracion();
            }
        }

        $valoracionMedia = $sumaValoraciones / $totalCapitulos;

        return $valoracionMedia;
    }
}
